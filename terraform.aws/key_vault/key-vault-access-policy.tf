# SaC Testing - Severity: Critical - set access_policy to undefined
resource "azurerm_key_vault_access_policy" "sac_key_vault_policy" {
  key_vault_id = azurerm_key_vault.sac_key_vault.id
  tenant_id    = data.azurerm_client_config.current.tenant_id
  object_id = ""  # Find user ID in AZ portal

  # SaC Testing - Severity: Critical - Set key_permissions to bad_key_permissions
  key_permissions = ["Delete", "Purge","Create", "Get", "Update"]
  # SaC Testing - Severity: Critical - Set secret_permissions to bad_secrets_permissions
  secret_permissions = ["Delete", "Purge", "Get", "Set", "List"]
  # SaC Testing - Severity: Critical - Set certificate_permissions to bad_certificates_permissions
  certificate_permissions = ["Delete", "DeleteIssuers", "Purge", "Create", "Get", "Update"]
}