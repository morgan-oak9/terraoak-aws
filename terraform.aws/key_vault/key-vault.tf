resource "azurerm_resource_group" "sac_key_vault_group" {
  name     = "sac-key-vault-group"
  location = "East US 2"
}

data "azurerm_client_config" "current" {}

resource "azurerm_key_vault" "sac_key_vault" {
  name                              = "sac-key-vault"
  location                          = azurerm_resource_group.sac_key_vault_group.location
  resource_group_name               = azurerm_resource_group.sac_key_vault_group.name
  tenant_id                         = data.azurerm_client_config.current.tenant_id
  # SaC Testing - Severity: Low - set sku_name != premium
  sku_name                          = "standard"
  # SaC Testing - Severity: Critical - set enable_rbac_authorization to false
  enable_rbac_authorization         = false
  # SaC Testing - Severity: Low - set soft_delete_retention_days >= 30
  soft_delete_retention_days        = 90
  # SaC Testing - Severity: High - set public_network_access_enabled to true
  public_network_access_enabled = true

  network_acls {
    bypass          = "AzureServices"
    # SaC Testing - Severity: Critical - set defualt_action != deny
    default_action  =  "Allow"
  }

  # SaC Testing - Severity: Moderate - Set tags to undefined
  # tags = {
  #   key = "value"
  # }
}

resource "azurerm_key_vault_key" "sac_key_vault_key" {
  name              = "sac-key-vault-key"
  key_vault_id      = azurerm_key_vault.sac_key_vault.id
  key_type          = "EC"

  # SaC Testing - Severity: High - Set curve to ""
  #curve = "P-256"
  # SaC Testing - Severity: Critical - Set exp to ""
  #expiration_date = "2006-01-02T15:04:05Z"
  # SaC Testing - Severity: High - Set nbf to ""
  #not_before_date = "2006-01-02T15:04:05Z"
  key_opts = ["sign","verify"]

  # SaC Testing - Severity: Moderate - Set tags to undefined
  # tags = {
  #   "key" = "value"
  # }
}

resource "azurerm_key_vault_secret" "sac_key_vault_secret" {
  name         = "sac-key-vault-secret-szechuan"
  value        = "szechuan"
  key_vault_id = azurerm_key_vault.sac_key_vault.id

  # SaC Testing - Severity: High - set not_before_date to ""
  #not_before_date = "2006-01-02T15:04:05Z"
  # SaC Testing - Severity: High - set expiration_date to ""
  #expiration_date = "2006-01-02T15:04:05Z"
  # SaC Testing - Severity: Low - set content_type to ""
  #content_type = "test"

  # SaC Testing - Severity: Moderate - Set tags to undefined
  # tags = {
  #   "key" = "value"
  # }
}

resource "azurerm_role_definition" "sac_keyvault_role" {
  name        = "sac-keyvault-create-key-secret-role"
  scope       = "/subscriptions/26e3ffed-afcb-4f7a-a34c-d7905542e0c4/resourceGroups/sac-key-vault-group"  # KeyVault Resource Group
  description = "This is a custom role created via Terraform"

  permissions {
    actions     = ["*"]
    data_actions = ["*"]
  }
}

resource "azurerm_role_assignment" "sac_keyvault_role_assignment" {
  name = "00482a5a-887f-4fb3-b363-3b7fe8e74483"   # Azure UUID
  scope                = "/subscriptions/26e3ffed-afcb-4f7a-a34c-d7905542e0c4/resourceGroups/sac-key-vault-group"
  principal_id         = "" # Find in AZ portal
  role_definition_name = "Owner"
}