resource "azurerm_resource_group" "mssql_database_resource_group" {
  name     = "example-resources"
  location = "East US 2"
}

resource "azurerm_mssql_database" "sac_mssql_database" {
  name           = "sac-mssql-database"     # Required
  server_id      = azurerm_mssql_server.mssql_database_server.id    # Required
  # SaC Testing - Severity: High - Set zone_redundant == false
  zone_redundant = false
  # SaC Testing - Severity: Critical - Set transparent_data_encryption == false
  transparent_data_encryption_enabled = false
  sku_name = "DW100c"
  create_mode = "Default"

  # SaC Testing - Severity: Moderate - Set tags to undefined
  tags = {
    author = "squirrel"
  }
}