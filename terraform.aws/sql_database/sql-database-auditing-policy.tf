# SaC Testing - Severity: Critical - Set auditing policy to undefined
resource "azurerm_mssql_database_extended_auditing_policy" "mssql_database_auditing_policy" {
  database_id = azurerm_mssql_database.sac_mssql_database.id   # Required

  # Validation requires enabled=True OR storage_endpoint and log_monitoring_enabled=true
  
  # SaC Testing - Severity: Moderate - Set enabled to false
  enabled = false    # Default true
  # SaC Testing - Severity: Moderate - Set enabled to false
  #storage_endpoint = azurerm_storage_account.example.primary_blob_endpoint
  # SaC Testing - Severity: Critical - Set log_monitoring_enabled  == false
  #log_monitoring_enabled = true

  # SaC Testing - Severity: Moderate - Set retention_in_days to < 90
  retention_in_days = 10
}