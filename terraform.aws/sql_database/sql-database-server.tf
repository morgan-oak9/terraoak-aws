resource "azurerm_mssql_server" "mssql_database_server" {
  name = "sac-testing-mssql-server"  # Required
  resource_group_name = azurerm_resource_group.mssql_database_resource_group.name   # Required
  location = azurerm_resource_group.mssql_database_resource_group.location  # Required
  version = "12.0"  # Required
  administrator_login = "msuch-oak9"  # Required
  administrator_login_password = "$uPer$ecure$ecret!234"  # Required

  # SaC Testing - Severity: Critical - Set minimum_tls_version != 1.2
  minimum_tls_version          = "1.2"
  # SaC Testing - Severity: High - Set public_network_access_enabled to true
  public_network_access_enabled = false

  # SaC Testing - Severity: Moderate - Set tags to undefined
  tags = {
    environment = "production"
  }
}