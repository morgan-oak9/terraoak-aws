resource "aws_db_proxy" "sac_rds_db_proxy" {
  name                   = "sac-rds-db-proxy"   # Required
  role_arn               = aws_iam_role.db_proxy_role.arn   # Required
  vpc_subnet_ids         = [aws_subnet.rds_subnet_1.id, aws_subnet.rds_subnet_2.id]  # Required
  engine_family          = "MYSQL"

  # SaC Testing - Severity: Moderate - Set debug_logging to true
  debug_logging          = false
  # SaC Testing - Severity: Moderate - Set require_tls to false
  require_tls            = true

  auth {  # Required
    secret_arn  = aws_secretsmanager_secret.sac_secrets_manager.arn   # Required
    # SaC Testing - Severity: High - Set auth_scheme != secrets
    auth_scheme = "SECRETS"
    # SaC Testing - Severity: High - Set iam_auth to non-preferred value
    iam_auth    = "REQUIRED"
  }

  # SaC Testing - Severity: Moderate - Set tags to undefined
  tags = {
    Name = "rds_db_proxy"
  }
}