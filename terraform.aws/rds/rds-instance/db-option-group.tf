resource "aws_db_option_group" "sac_rds_option_group" {
  name                     = "sac-rds-option-group"
  option_group_description = "Terraform Option Group"
  engine_name              = "mysql"
  major_engine_version     = "8.0"

  # SaC Testing - Severity: Moderate - Set tags to undefined
  tags = {
    Name = "rds_option_group"
  }
}