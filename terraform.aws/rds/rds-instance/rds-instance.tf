resource "aws_db_instance" "sac_db_instance" {
  db_name                 = "sacDatabaseName"
  identifier              = "sac-testing-db-instance"  # Required
  allocated_storage       = 10  # Required
  instance_class          = "db.t3.micro"  # Required
  username                = "sacRDSInstanceName"
  
  password                = "randomPasswordThatFollowstheCharLimit" # Required
  engine                  = "mysql"
  skip_final_snapshot    = true
  final_snapshot_identifier = "DELETE"
  db_subnet_group_name = aws_db_subnet_group.sac_rds_subnet_group.name
  deletion_protection     = false


  # SaC Testing - Severity: High - Set engine to unsupported version
  engine_version          = "8.0"
  # SaC Testing - Severity: High - Set enabled_cloudwatch_logs_exports to []
  enabled_cloudwatch_logs_exports = ["audit", "error", "general", "slowquery"]
  # SaC Testing - Severity: Low - Set monitoring_interval to undefined
  monitoring_interval = 10
  monitoring_role_arn = aws_iam_role.sac_instance_monitoring_role.arn
  # SaC Testing - Severity: High - Set iam_database_authentication_enabled to false
  iam_database_authentication_enabled = true
  # SaC Testing - Severity: High - Set kms_key_id to false
  kms_key_id = aws_kms_key.sac_kms_key.arn
  # DEMO - severity change based on business impact Low -> High
  # SaC Testing - Severity: Moderate - Set multi_az to false
  multi_az = true
  # SaC Testing - Severity: Critical - Set publicly_accessible to true
  publicly_accessible     = false
  # DEMO - severity change based on business impact/data sensitivity Moderate -> Critical
  # SaC Testing - Severity: Moderate - Set storage_encrypted to false
  storage_encrypted = true
  
  # SaC Testing - Severity: Moderate - Set tags to undefined
  tags = {
    Name   = "rds_cluster"
  }
}

resource "aws_db_proxy_default_target_group" "sac_proxy_target_group" {
  db_proxy_name = aws_db_proxy.sac_rds_db_proxy.name
}

resource "aws_db_proxy_target" "sac_instance_proxy_target" {
  db_proxy_name = aws_db_proxy.sac_rds_db_proxy.name
  target_group_name = aws_db_proxy_default_target_group.sac_proxy_target_group.name
  db_instance_identifier = aws_db_instance.sac_db_instance.id
}

resource "aws_vpc" "rds_vpc" {
  cidr_block = "10.0.0.0/16"
  tags = {
    Name = "sac-rds-vpc"
  }
}

resource "aws_subnet" "rds_subnet_1" {
  vpc_id     = aws_vpc.rds_vpc.id
  cidr_block = "10.0.0.0/24"
  availability_zone = "us-east-2c"

  tags = {
    Name = "Main"
  }
}

resource "aws_subnet" "rds_subnet_2" {
  vpc_id     = aws_vpc.rds_vpc.id
  cidr_block = "10.0.2.0/24"
  availability_zone = "us-east-2b"

  tags = {
    Name = "Main"
  }
}

resource "aws_iam_role" "db_proxy_role" {
  name = "rds_role"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "rds.amazonaws.com"
        }
      },
    ]
  })
  tags = {
    key = "tag-value"
  }
}

resource "aws_iam_role" "sac_instance_monitoring_role" {
  name = "sac-testing-rds-cloudwatch-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "monitoring.rds.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "rds_enhanced_monitoring" {
  role       = aws_iam_role.sac_instance_monitoring_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonRDSEnhancedMonitoringRole"
}

resource "aws_secretsmanager_secret" "sac_secrets_manager" {
  name                    = "sac-testing-secrets-manager-01"
  description             = "Default config2"

  # SaC Testing - Severity: Moderate - Set kms_key_id to undefined
  kms_key_id = aws_kms_key.sac_kms_key.id
  recovery_window_in_days = 10 
  
  # SaC Testing - Severity: Moderate - Set tags to undefined
  tags = {
    Env = "dev"
  }
}

resource "aws_secretsmanager_secret_policy" "sac_secrets_manager_policy" {
  secret_arn = aws_secretsmanager_secret.sac_secrets_manager.arn

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "EnableAnotherAWSAccountToReadTheSecret",
      "Effect": "Allow",
      "Principal": "*",
      "Action": "*",
      "Resource": "*"
    }
  ]
}
POLICY
}

resource "aws_kms_key" "sac_kms_key" {
  description             = "This key is used to encrypt dynamoDB objects"
  deletion_window_in_days = 30
  # SaC Testing - Severity: Critical - Set enable_key_rotation to false
  enable_key_rotation = true
  # SaC Testing - Severity: Critical - Set enabled to false
  #enabled = true
  # SaC Testing - Severity: Moderate - Set key_usage != ['sign_verify' 'encrypt_decrypt']
  key_usage = "ENCRYPT_DECRYPT"
  customer_master_key_spec = "RSA_2048"
  # SaC Testing - Severity: Moderate - Set tags to undefined
  tags = {
    Name        = "kms-key-1"
  }
}